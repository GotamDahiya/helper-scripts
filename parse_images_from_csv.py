'''parse_images_from_csv.py
=======================

Usage
-----
python[3] parse_images_from_csv.py [csv file] [output directory] [labels] [column]

csv file -> Path to CSV file where images are stored
output directory -> Path to directory for storing images in class folders
labels -> Class labels. If the class labels are binarized in the CSV file, they should be in the same order.
column -> 1-indexed Column number where the class name/label is defined.
'''

# importing the necessary libraries
import pandas as pd
import numpy as np
import os, argparse, random, pickle, json

from PIL import Image

# function for parsing the images from a given CSV file
def parse_from_csv(csv_file,output_directory,classes,idx):
    # Reading the CSV file using pandas
    data = pd.read_csv(csv_file)
    labels = data[idx] # Creating pandas series storing the class labels/names
    images = data.drop(columns=(idx-1)) # Extracting the columns containg the pixel values for each image 

    # Extracting image shape from number of columns present in images dataframe.
    # Eg: images.shape[1] = 784, img_shape = 28
    img_shape = int(images.shape[1]**0.5)
    images = images.values.reshape(-1,img_shape,img_shape)

    # Keeping count of images present in each class 
    count_classes = {}
    for k in np.unique(df.values):
        count_classes[k] = 0
        pass

    # Changing working directory to output directory.
    os.chdir(output_directory)

    # Looping over each image present in the numpy array "images"
    for idx, row in enumerate(images):
        # If class names are binarized this try-except block changes it to the corresponding class label.
        try:
            label = int(labels.loc[idx])
            label = classes[label] # converting from binarized value to encoded value.
            count_classes[int(labels.loc[idx])] += 1
        except:
            label = labels.loc[idx]
            count_classes[label] += 1
        pass
        
        if not os.path.exists(label):
            os.makedirs(label)

        # Updating the filename to this format "class-label__count".
        filename = "{}_{:06d}".format(label,count_classes[label])
        outputPath = os.path.join(output_directory,label,filename)
        img = Image.fromarray(row)
        img.save(outputPath+'.png') # Saving image in class folder
        pass

    print("[INFO] total number of images stored in each class {}".format(json.dumps(count_classes,indent=4)))
    print("[INFO] All files are stored in {}".format(output_directory))

    pass

# Calling command line arguments
ap = argparse.ArgumentParser()
ap.add_argument("-c","--csv-file",type=str,required=True,
    help='Path to CSV file for parsing images from.')
ap.add_argument("-o","--output-directory",type=str,
    help="Path to output directory for storing images")
ap.add_argument("-l","--labels",type=str,required=True,
    help="Comma separated list of labels")
ap.add_argument("-i","--column",required=True,
    help="1-indexed column number for label/class name")

args = vars(ap.parse_args())

# If output directory is not present, default changes to the current working directory
if args["output_directory"] == None:
    args["output_directory"] = os.getcwd()

if args["directory"] == None:
    print("Path to directory or CSV file should be present")
    sys.exit()

output_directory = os.path.abspath(args["output_directory"])
directory = os.path.abspath(args["directory"])
csv_file = os.path.abspath(args["csv_file"])

# If the output directory does not exist, it is made
if not os.path.exists(output_directory):
    os.makedirs(output_directory)

labels_list = [i for i in args["labels"].split(",")]
labels_names = {} # Creating a dictionary for converting the binarizer values to encoded values.

for (i,k) in enumerate(labels_list):

    labels_names[i] = k
    pass

# Calling the function
parse_from_csv(csv_file,output_directory,labels_names,args["column"])
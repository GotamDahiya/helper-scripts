numpy==1.18.5
numpydoc==1.1.0
pandas==1.1.0
opencv-contrib-python==4.4.0.44
rasterio==1.1.7
urllib3==1.24.3
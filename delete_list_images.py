'''delete_list_images.py
====================

Usage
-----
python[3] delete_list_images.py [input directory] [output directory]

input directory -> Path to directory where images are stored.
output directory -> Path to store the list of images to be deleted.

a,d -> Navigation keys; "a" one image to the left; "d" one image to the right
w -> Add filename to delete list
r -> Remove filename from the delete list
q -> Exit program
'''
import numpy as np
import pandas as pd

import argparse, os, cv2, random, re

def delete_list(directory, output_directory):

    directory = os.path.abspath(directory)
    output_directory = os.path.abspath(output_directory)

    file_types = (".png",".jpg",".svg")
    file_list = [files for files in os.listdir(directory) if files.endswith(file_types)]
    file_list.sort(key=lambda f: int(re.sub('\D','',f)))

    del_lst = []
    l, i = len(file_list), 0

    class_name = directory.split(os.sep)[-1]
    print("Number of files in {} is {}".format(directory,l))
    
    while i < l:
        img = cv2.imread(os.path.join(directory,file_list[i]))

        white_blank = 255 * np.ones([48,300,3],dtype=np.uint8)

        cv2.putText(white_blank,"{}".format(file_list[i]), (10,30), cv2.FONT_HERSHEY_SIMPLEX,0.5,(255,0,0),1)

        cv2.imshow("Image", img)
        cv2.imshow("Filename", white_blank)

        key = cv2.waitKey(0)

        if key == ord('w'):
            del_lst.append(os.path.join(directory,file_list[i]))
            print("Adding file {} to delete list".format(file_list[i]))
        elif key == ord("d"):
            try:
                i += 1
                file = file_list[i]
            except:
                print("No more files to the right")
                continue
        elif key == ord('a'):
            try:
                i -= 1
                file = file_list[i]
            except:
                print("No more files to the left")
                continue
        elif key == ord('r'):
            del_lst.remove(os.path.join(directory, file_list[i]))
            print("Removing file {} from delete list".format(file_list[i]))
        elif key == ord('q'):
            break
        pass
    
    cv2.destroyAllWindows()

    if del_lst:
        with open(os.path.join(output_directory,"del_lst_{}.txt".format(class_name)),"w") as output:
            for row in del_lst:
                output.write(str(row)+"\n")
            pass
    pass

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument("-d","--directory",type=str,required=True,
        help="Path to dataset from which images have to be removed.")
    ap.add_argument("-o","--output-directory",type=str,required=True,
        help="Path to output directory")
    args = vars(ap.parse_args())

    args["directory"] = os.path.abspath(args["directory"])
    args["output_directory"] = os.path.abspath(args["output_directory"])

    delete_list(args["directory"],args["output_directory"])
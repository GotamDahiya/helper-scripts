'''split_labels_csv.py
===================

Usage
-----
python[3] split_labels_csv.py [input images directory] [labels] [output-directory] [separator]

input images directory -> directory containing the ungrouped files
labels -> class names of the dataset
output-directory -> directory for storing the CSV file
separator -> regex condition for separating the class label from the filename.

If there are any leading zeros present in the label part of the filename, they are autumatically removed.

No need to bother whether it is class label or name present with, the filename, the script takes care of that.
'''

# importing the necessary libraries
import numpy as np
import pandas as pd
import cv2, os, argparse, pickle, json, random, re

ap = argparse.ArgumentParser()
ap.add_argument("-d","--directory",required=True,
    help="Path to dataset directory[preferred absolute path]")
ap.add_argument("-l","--labels",required=True,type=str,
    help="comma separated list of class names")
ap.add_argument("-o","--output-directory",type=str,
    help="Path to output directory[preferred absolute path")
ap.add_argument("-s","--separator",required=True,
    help='Separator for splitting of filename to obtain class name and label')

args = vars(ap.parse_args())

# If output directory is not set, it defaults to the present working directory.
if args["output_directory"] == None:
    args["output_directory"] = os.getcwd()

# Changing all paths to absolute paths for easier use while reading and writing objects.
if not os.path.isabs(args["directory"]):
    args["directory"] = os.path.abspath(args["directory"])

if not os.path.isabs(args["output_directory"]):
    args["output_directory"] = os.path.abspath(args["output_directory"])

# Splitting the comma separated list of class names
labels_list = [i for i in args["labels"].split(',')]

# Creating dictionaries for storing class names and corresponding labels
labels_name, labels = {}, {}

for (i,k) in enumerate(labels_list):

    labels_name[i] = k
    labels[k] = i
    pass

print(labels_names, labels)

# Pandas dataframe for storing the filename and corresponding class labels and names
labels_df = pd.DataFrame(columns=['Filename','Label','ClassName'])

for filename in os.listdir(args["directory"]):
    label = filename.split(args["separator"])[0]
    label = re.sub(r'0+(.+)',r'\1',label)

    # converting the label to an integer. If class name is given, then it the corresponding label is found using the above dictionaries.
    try:
        label = int(label)
    except:
        label = labels[label]

    # Appending the filename, label, class name to the dataframe
    labels_df = labels_df.append({"Filename":filename,"Label":label,"ClassName":labels_name[label]},ignore_index=True)
    pass

# Converting the dataframe to CSV and savaing it in the output directory
labels_df.to_csv(os.path.join(args["output_directory"],"labels-df.csv"))

# Saving the class names and the corresponding labels inside a JSON file.
labels_name = json.dumps(labels_name,indent=4)

with open(os.path.join(args["output_directory"],'label_map.json'),"w") as outfile:

    outfile.write(labels_name)
    pass

print("Files are stored in {}".format(args["output_directory"]))
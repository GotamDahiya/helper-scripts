'''augment_images.py
=================

Usage
-----
python[3] augment_images.py [input directory] [number of times each image has to be augmented]

For directories absolute path preferred.

Each original image will be augmented using the imgaug library randomly.
Currently the following augmentations are implemented -
    1. Fliplr
    2. Crop
    3. LinearContrast
    4. Multiply
    5. Affine
'''

# importing the necessary libraries
import numpy as np
import pandas as pd
import imgaug.augmenters as iaa
import matplotlib.pyplot as plt
import tifffile as tiff
import imgaug.imgaug
import os, re, json, random, argparse, cv2

from PIL import Image

# function for augmenting the images present in a dataset
def augmentImages(directory: str, number: int):

    # changing to absolute path
    directory = os.path.abspath(directory)

    # Various Augmenting methods.
    seq = iaa.Sequential([
        iaa.Fliplr(0.5),
        iaa.Crop(percent=(0,0.2)),
        iaa.LinearContrast((0.75,1.5)),
        iaa.Multiply((0.8,1.2), per_channel=0.2),
        iaa.Affine(
            scale={'x':(0.8,1.2), "y":(0.8,1.2)},
            translate_percent={"x":(-0.2,0.2),"y":(-0.2,0.2)},
            rotate=(-25,25),
            shear=(-8,8))
        ], random_order=True)

    # Changing the current working directory to input directory
    # os.chdir(directory)

    # Listing all the original images in a class directory
    list_files = os.listdir(directory)
    list_files.sort(key=lambda f: int(re.sub('\D','',f)))

    for img_file in list_files:

        # Reading an image
        img = cv2.imread(os.path.join(directory,img_file))

        # Looping over the required number of times the image has to be augmented
        for i in range(number):

            # Creating an augmented image
            aug_img = seq.augment_image(image=img)
            filename = "{}_{}.png".format(img_file.split(".")[0], (i+1))

            # Writing the augmented image with the filename as follows "orginal-filename_i.png"
            cv2.imwrite(os.path.join(directory, filename), aug_img)
            pass
        pass

    print("[INFO] Original and augmented images can be found at {}".format(directory))
    pass

# In case the program is called from the command line
if __name__ == "__main__":

    # Initialising the argument parser
    ap = argparse.ArgumentParser()
    ap.add_argument("-d","--directory",type=str,required=True,
        help="Path to images directory") 
    ap.add_argument("-n","--number",type=int,required=True,
        help="Number of times an image has to augmented")
    args = vars(ap.parse_args())

    # Calling the function
    augmentImages(args["directory"],args["number"])
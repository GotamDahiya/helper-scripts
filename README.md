# Scripts for Cleaning Data

This project contains scripts for automating process of cleaning data, such as renaming filenames to follow a set convention, spliting of data into various class folders, creating a train,test and validation CSV files, and many more.

## Scripts

Serial Number | File | Explanation | Use Case
------------- | ---- | ----------- | --------
1 | rename_files_dir | Renaming of files present in a directory. Convention followed for renaming file is **label-name\_file-number**. File number is updated using a counter. | When files are already separated into respective class folders.
2 | split_labels_dir | Splitting files into respective class folders. Supports renaming of file using *1* conventions| When data is not gropued into various class folders. Useful when using a data generator.
3 | prepare_train_test_valid_csv | Creates a CSV file containing filenames, label and class names pertaining to the training, testing and validation datasets. | Split a dataset when class folders are present or based upon a CSV file given which contanins the filenames and corresponding class names and labels.
4 | split_labels_csv | Creates a CSV file for storing the file along with their respective classes and labels for extraction. This reduces hard disk usage, which is beneficial for massive datasets. More directories are not made which make it ideal for them. | Use when hard disk space is a major issue or for massive datasets where splitting into respective folders may take time.
5 | parse_images_from_csv | Creates directories to store images present in a CSV file. Separate folders are made for each class. | When images are stored in a CSV file and need to be split into various class folders.
6 | download_images_flickr | Used for downloading images from *Flickr* using a query as given by the user | For creating image databases for classification, object detection and recognition.
7 | delete_list_images | Use for creating a list of image filenames to be deleted from a dataset | After obtaining the data, the images which are not relevant to their respective classes can be deleted using the list generated from the terminal.
8 | augment_images | Used for augmenting the images present in a dataset. | Used when the number of original images are quite low or need to balance the classes using augmentation.
9 | prepare_train_test_valid_folder | Creating the training, testing and validation folders containing the respective images from the various classes | To be used for creating a data generator without depending upon the train,test and validation CSV files.
10 | download_images_unsplash | Used for downloading images from *Unsplash* using a query as given by the user | Same as *download_images_flickr*

## Future Work

Serial Number | File | TODO
------------- | ---- | ----
1 | split_labels_dir | Find classes based on folder names.
2 | prepare_train_test_valid_csv | To find labels with respect to folder names preserving binarizer. Splitting of script into 2 different files.
3 | parse_images_to_csv | Creating a script for storing images in a CSV file. Required a common image size[Square].

<s>* Add a GUI for all the files using TKinter for easier use.</s>
<s>* Instead of using TKinter, Flask can be used for easier usage. </s>

## Update

1. split_labels_csv &#8594; Allows for making a CSV file for splitting the dataset without creating new directories. Ideal for massive datasets.
2. prepare_train_test_valid.csv &#8594; Allows for updating the CSV file made from the above script to include labels for training, testing and validation.
3. parse_images_from_csv &#8594; Allows for creating various class folders for images stored in a CSV file.
4. delete_list_images &#8594; Creates a list of images to be deleted from the class folder.
5. Converted all the Pickle statements to JSON statements for interoperability between languages.
6. augment_images &#8594; Augments the images present in a directory.
7. prepare_train_test_valid_folder &#8594; Extension of prepare_train_test_valid_csv, by creating the training, testing and validation datasets using the CSV files generated by that script.

## Getting Started

Follow the proceding instructions to use this repository.

### Prerequistes

Install these software for a good run
```
Python 3.8.0 or higher
```
Libraries required for the project
```
Numpy 
Os
Pickle
Argparse
```

Follow the requirements.txt file to download the above libraries
```
pip[3] install -r requirements.txt
```

## Built With

* **Python v3.8.2**

## Authors

* [*Gotam Dahiya*](https://gitlab.com/GotamDahiya) 
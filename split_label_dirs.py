'''split_label_dirs.py
===================

Usage
-----
python[3] split_label_dirs.py [input images directory] [output directory] [labels]

For directories use absolute path path.

Labels can be any order. Depending on the order the class labels will be defined. They are 0-indexed.

Example,
Arguments passed : no-ship, ship
Output: 0_123, 1_123; 0 -> no-ship, 1 -> ship
Arguments passed : ship , no-ship
Output: 0_123, 1_123; 1 -> ship, 0 -> no-ship 

TODO1: Find classes based on folder names.
'''

# This script automatically renames the files while splitting the data into the various folders.
# It renames the files in this fashion: "label-name_serial-number"


# importing the necessary libraries
import numpy as np
import pandas as pd
import  os, argparse, pickle, json, re

from PIL import Image

# Calling the command line arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d","--directory",required=True,
    help="Path to where images are stored [preferred absolute path]")
ap.add_argument("-o","--output-directory",required=True,
    help="Path to the output directory [preferred absolute path]")
ap.add_argument("-l","--labels",required=True,type=str,
    help="comma separated list of class names")
ap.add_argument("-s","--separator",required=True,type=str,
    help="Separator for splitting of filename to obtain class name and label")

args = vars(ap.parse_args())

# Converting all paths to absolute paths
args["directory"] = os.path.abspath(args["directory"])

args["output"] = os.path.abspath(args["output_directory"])

data_path = os.path.join(args["directory"])
out_path = os.path.join(args["output_directory"])


labels_list = [i for i in args["labels"].split(",")]
labels_name, labels_count, labels = {}, {}, {} # Creating a dictionary for encoding the class names and the total number of images per class.

for (i,k) in enumerate(labels_list):

    labels_name[i] = k
    labels[k] = i
    labels_count[i] = 0
    pass


# print(labels_name,labels_count)

# Looping over the images present in the dataset.
for image in os.listdir(data_path):

    # extracing the binarized class label 
    img_label = image.split(args["separator"])[0]
    label = re.sub(r'0+(.+)',r'\1',img_label)

    # converting the label to an integer. If class name is given, then it the corresponding label is found using the above dictionaries.
    try:
        label = int(label)
    except:
        label = labels[label]


    print("[INFO] loading image {} with label {}".format(image, labels_name[label]))
    
    # Creating a new filename with standard format class-label_count
    filename = "{}_{:06d}.png".format(labels_name[label],labels_count[label])

    out_dir = os.path.join(out_path,labels_name[label])
    # Creating class folder if it does not exist.
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # Saving the image to the class folder.
    outputPath = os.path.join(out_dir,filename)
    inputPath = os.path.join(data_path,image)

    try:
        # Verifying if the image is not corrupted.
        img = Image.open(inputPath)
        img.verify()
        img.close()
        # Testing if the image has not been repeated.
        if not os.path.exists(outputPath):
            os.rename(inputPath,outputPath)
            labels_count[label] += 1
    except:
        print("[INFO] Image {} with label {} is corrupted".format(image,labels_name[label]))
    pass

# Saving the class names and the corresponding labels inside a JSON file.
labels_name = json.dumps(labels_name,indent=4)

with open(os.path.join(args["output_directory"],'label_map.json'),"w") as outfile:

    outfile.write(labels_name)
    pass

print("Files are stored in {}".format(args["output_directory"]))
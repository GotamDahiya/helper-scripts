import numpy as np
import datetime as dt
import pandas as pd
import gdal, rasterio, imageio, argparse, os, re, warnings

warnings.filterwarnings("ignore")

def convert_tiff_jpg(directory):

    directory = os.path.abspath(directory)
    os.chdir(directory)    

    for sub_dir in os.listdir(directory):

        list_files = os.listdir(sub_dir)
        list_files.sort(key=lambda f: int(re.sub('\D','',f)))

        for tif in list_files:
            
            src = rasterio.open(os.path.join(sub_dir,tif))
            arr1, arr2, arr3 = src.read(1), src.read(2), src.read(3)
            
            arr1 = np.array(arr1, dtype=np.float32)
            arr2 = np.array(arr2, dtype=np.float32)
            arr3 = np.array(arr3, dtype=np.float32)


            arr1 = (arr1 - arr1.min()) * (255/(arr1.max() - arr1.min()))
            arr2 = (arr2 - arr2.min()) * (255/(arr2.max() - arr2.min()))
            arr3 = (arr3 - arr3.min()) * (255/(arr3.max() - arr3.min()))

            # rgbArray = np.zeros((arr1.shape[0], arr1.shape[1],3), 'uint16')
            rgb = np.dstack((arr1,arr2,arr3)).astype(np.uint8)

            filename = tif.split(".")[0]
            imageio.imwrite(os.path.join(sub_dir, "{}.png".format(filename)), rgb)
            pass

        print("[INFO] Finished converting for {}".format(sub_dir))
        pass

    print("[INFO] finished converting for {}".format(directory))
    pass


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument("-d","--directory",type=str,required=True,
        help="Path to dataset for for converting .tif to .jpg")
    args = vars(ap.parse_args())

    convert_tiff_jpg(args["directory"])
'''prepare_train_test_valid_folder.py
==================================

Usage
-----
python[3] prepare_train_test_valid_folder.py [path to images directory] [comma separated paths to train,test and validation csv files] [output directory]

For all directories and paths, absolute paths are preferred.

Creates training, validation and testing datasets of images for all images stored in the training, testing and validation CSV files.
'''

# importing the necessary libraries
import pandas as pd
import numpy as np
import os, json, random, argparse, cv2, imageio

from PIL import Image

# function for preparing the dataset folders
def prepareFolders(directory,output_directory,files):

    # Converting to absolute paths
    img_path = os.path.abspath(directory)
    out_path = os.path.abspath(output_directory)

    # Looping over the CSV files
    for file in files:

        # Converting to absolute path
        file_path = os.path.abspath(file)
        
        # Extracting the type of dataset from path. Tail->train.csv
        head, tail = os.path.split(file_path)
        dataset = tail.split(".")[0]
        
        # Creating the folder if it does not already exist
        if not os.path.exists(os.path.join(out_path, dataset)):
            os.makedirs(os.path.join(out_path, dataset))

        # Creating variable save_path for easier usage
        save_path = os.path.join(out_path, dataset)

        # Reading the CSV file using panda's "read_csv" function
        df = pd.read_csv(file_path,index_col="Unnamed: 0")

        # Looping over all the rows in the CSV file
        for i in range(df.shape[0]):

            # Extracting the class path to file
            img_filename = df.loc[i,"Filename"]
            # REading the image
            img = imageio.imread(os.path.join(directory,img_filename))

            # Obtaining to which class image belongs to
            sub_dir = img_filename.split("/")[0]
            # Creating the class folder if it does not exist already
            if not os.path.exists(os.path.join(save_path,sub_dir)):
                os.makedirs(os.path.join(save_path,sub_dir))

            # Writing the image to the specific class folder in the dataset
            imageio.imwrite(os.path.join(save_path,img_filename),img)
            pass
        
        print("[INFO] Finished creating {} dataset".format(dataset))
        pass

    print("[INFO] Finished splitting into training, validation and testing datasets")
    pass

# In case the program is called from the command line
if __name__ == "__main__":

    # Initialising the argument parser
    ap = argparse.ArgumentParser()
    ap.add_argument("-d","--directory",type=str,required=True,
        help="Path to input dataset")
    ap.add_argument("-c","--csv",type=str,required=True,
        help="Comma separated paths to train,validation and test CSV files")
    ap.add_argument("-o","--output-directory",type=str,required=True,
        help="Path to output directory")
    args = vars(ap.parse_args())

    # Splitting the path into a list from a comma separated string
    csv_files = [i for i in args["csv"].split(",")] 

    # Calling the function
    prepareFolders(args["directory"], args["output_directory"], csv_files)
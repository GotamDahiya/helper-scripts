'''rename_files_dir.py
==================

Usage
-----
python[3] [directory path]

For directories use absolute path.

It renames the files already in present in a class folder.
'''

# importing the necessary libraries
import os, argparse

# Calling command line arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d","--directory",required=True,
    help="absolute path to dataset")
args = vars(ap.parse_args())

args["directory"] = os.path.abspath(args["directory"])
# Obtaining folders list from directory
data_dir_list = os.listdir(args["directory"])

# looping over the folders
for data_dir in data_dir_list:

    print('[INFO] Renaming files from folder: {}'.format(data_dir))
    # Obtaining filenames list from folder
    data_list = os.listdir(os.path.join(args["directory"],data_dir))
    # Changing directory to current folder
    os.chdir(os.path.join(args["directory"],data_dir))
    # Base name is set as folder name which is the class name.
    base_name = data_dir
    
    for i in range(len(data_list)):
        img_name = data_list[i]

        # Renaming file in the order class-label_xxxxxx.png
        img_rename = base_name+'_{:06d}'.format(i+1)+'.png'
        # If file already exists skip it.
        if not os.path.exists(img_rename):
            os.rename(img_name,img_rename)
        pass

    print("[INFO] completed renaming folder: {}".format(data_dir))
    pass

print("[END] Completed renaming all files at: {}".format(args["directory"]))
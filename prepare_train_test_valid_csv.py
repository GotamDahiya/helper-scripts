'''prepare_train_test_valid_csv.py
===============================

Usage
-----
python[3] prepare_train_test_valid_csv.py [input classes and images directory] [labels]

For directories use absolute path instead of relative paths to avoid confusion.

Labels should be in the order of the filenames. They are 0-indexed. This is a good convention to follow as it allows the class names and their repective binary formats to remain the same.

Creates a pickle file to store the classes with their corresponding labels.

TODO1: To find labels with respect to folder names preserving binarizer. 
'''

# Need to split into different methods for different use cases.

# importing the necessary libraries
import numpy as np
import pandas as pd
import random, os, argparse, json

from PIL import Image

# function for splitting into training, validation and testing from folder
def splittingFromFolder(directory,output_directory,labels_name):

    data_path = os.path.join(directory)
    data_dir_list = os.listdir(data_path)
    print("[INFO] data list is: ",data_dir_list)

    num_classes = len(labels_name)

    # Creating dataframes for storing which all images go to training, validation and testing datasets.
    train_df = pd.DataFrame(columns=['Filename','Label','ClassName'])
    test_df = pd.DataFrame(columns=['Filename','Label','ClassName'])
    validation_df = pd.DataFrame(columns=['Filename','Label','ClassName'])

    # Looping over each class in the original dataset.
    for dataset in data_dir_list:
        # Obtaining filenames list.

        file_types = (".png",".jpg",".svg",".tif")
        img_list = [file for file in os.listdir(os.path.join(args["directory"],dataset)) if file.endswith(file_types)]
        print("[INFO] loading images of dataset: {}".format(os.path.join(directory,dataset)))
        label = labels_name[dataset]

        # Randomly shuffling the filenames
        num_img_files = len(img_list)
        num_corrupted_files = 0
        random.shuffle(img_list)

        # Creating a training, validation and testing split. Hardcoded Parameters = 0.7,0.2,0.1
        train_pos = int(0.7*num_img_files)
        test_pos = int(0.1*num_img_files)
        val_pos = int(0.2*num_img_files)

        train_data = img_list[:train_pos]
        val_data = img_list[train_pos:(train_pos+val_pos)]
        test_data = img_list[-test_pos:]

        for i in img_list:

            img_filename = os.path.join(dataset,i)
            img_name = os.path.join(data_path,dataset,i)
            # try-except block for filtering corrupt images from the dataset.
            try:
                # Verifying if the image is not corrupted
                # input_img = Image.open(img_name)
                # input_img.verify()
                # input_img.close()
                
                # Saving the filename, binarized class name and class name to training set
                if i in train_data:
                    train_df = train_df.append({'Filename':img_filename,"Label":label,"ClassName":dataset},ignore_index=True)
                # Saving the filename, binarized class name and class name to validation set
                if i in val_data:
                    validation_df = validation_df.append({'Filename':img_filename,"Label":label,"ClassName":dataset},ignore_index=True)
                # Saving the filename, binarized class name and class name to testing set
                if i in test_data:
                    test_df = test_df.append({'Filename':img_filename,"Label":label,"ClassName":dataset},ignore_index=True)
            except:
                print("{} is corrupted".format(img_filename))
                num_corrupted_files += 1

        print("[INFO] Read {} images out of {} images from data directory {}".format(num_img_files-num_corrupted_files,num_img_files,dataset))
        pass
    print("[INFO] completed reading from all images from dataset")

    train_df = train_df.reindex(np.random.permutation(train_df.index))
    validation_df = validation_df.reindex(np.random.permutation(validation_df.index))
    test_df = test_df.reindex(np.random.permutation(test_df.index))

    # Saving the training, validation, and testing splits to CSV file.
    train_df.to_csv(os.path.join(output_directory,"train.csv"), index=False)
    validation_df.to_csv(os.path.join(output_directory,'validation.csv'), index=False)
    test_df.to_csv(os.path.join(output_directory,'test.csv'), index=False)

    # Saving the binarizer to a JSON file.
    labels_name = json.dumps(labels_name,indent=4)

    with open(os.path.join(output_directory,'label_map.json'),"w") as outfile:

        outfile.write(labels_name)
        pass


    print("[INFO] Finished splitting files into training, validation and testing datasets. JSON file contianing the label binarizer is also made. The CSV files can be found at {}".format(output_directory))
    pass

# function for splitting into training, validation and testing from a CSV file.
def splittingCSV(csvFile,output_directory,labels_name):

    data = pd.read_csv(os.path.join(csvFile))
    data['Dataset Label'] = " "
    random.seed(42)

    # Obtain indexes for the respective classes assigning train, test and validation dataset label
    for c in labels_name.keys():
        # Obtaining indexes where the class name is equal to the present key
        idx = np.array(data.index[(data['ClassName'] == c)])
        random.shuffle(idx)

        len_idx = idx.shape[0]

        # Calculating training, validation and testing split using number of indexes found in the above step
        train = int(0.7*len_idx)
        val = int(0.2*len_idx)
        test = int(0.1*len_idx)

        train_idx = idx[:train]
        val_idx = idx[train:(val+train)]
        test_idx = idx[-test:]

        # Setting the label of training, validation and testing within the original CSV file
        data.loc[train_idx,'Dataset Label'] = "Training"
        data.loc[val_idx,"Dataset Label"] = "Validation"
        data.loc[test_idx,"Dataset Label"] = "Testing"

        print("[INFO] completed splitting of {}".format(c))
        pass
    
    # Saving to specified file.
    csvName = csvFile.split(os.sep)[-1]
    data.to_csv(os.path.join(output_directory,csvName))

    # Saving the binarizer to a JSON file.
    labels_name = json.dumps(labels_name,indent=4)

    with open(os.path.join(output_directory,'label_map.json'),"w") as outfile:

        outfile.write(labels_name)
        pass

        
    print("[INFO] Finished splitting of dataset into training, validation and testing datasets. JSON file contianing the label binarizer is also made. The CSV file can be found at {}".format(output_directory))
    pass

# Calling the command line arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d","--directory",
    help="path to dataset folder")
ap.add_argument("-c","--csv-file",type=str,
    help="path to CSV file with filenames and corresponding class names/labels")
ap.add_argument("-l","--labels",type=str,required=True,
    help="comma separated list of class names")
ap.add_argument("-o","--output-directory",type=str,
    help="path to directory for storing CSV files")

args = vars(ap.parse_args())

# Setting default output directory to current working directory 
if args["output_directory"] == None:
    args["output_directory"] = os.getcwd()

labels_list = [i for i in args["labels"].split(",")]
labels_name = {} # Creating a dictionary for binarizing the class names.

for (i,k) in enumerate(labels_list):

    labels_name[k]=i;
    pass

if not args["directory"] == None and not args["csv_file"] == None:
    print("Give either CSV file or dataset containing grouping of data. Do not give both")
    sys.exit()

output_directory = os.path.abspath(args["output_directory"])

if args["directory"] == None and args["csv_file"] == None:
    print("Give either CSV file or dataset containing grouping of data")
    sys.exit()

# Creating output directory, if does not exist
if not os.path.exists(args["output_directory"]):
    os.makedirs(args["output_directory"])

# For folder
if not args["directory"] == None:
    directory = os.path.abspath(args["directory"]) # converting to absolute path
    splittingFromFolder(directory,output_directory,labels_name)

# For CSV file
if not args["csv_file"] == None:
    csvFile = os.path.abspath(args["csv_file"]) # converting to absolute path
    splittingCSV(csvFile,output_directory,labels_name)

print("Files are stored in {}".format(args["output_directory"]))